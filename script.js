let number1=document.getElementById("value1");
let number2=document.getElementById("value2");
let number3=document.getElementById("value3");
let number4=document.getElementById("value4");
let number5=document.getElementById("value5");
let number6=document.getElementById("value6");
let number7=document.getElementById("value7");
let number8=document.getElementById("value8");
let number9=document.getElementById("value9");
let number0=document.getElementById("value0");
let decimalPoint=document.getElementById("value1");

let plus=document.getElementById("operator-+");
let minus=document.getElementById("operator--");
let into=document.getElementById("operator-*");
let quotient=document.getElementById("operator-/");
let equalsto=document.getElementById("equalsto");
let clear=document.getElementById("clear");

let display=document.getElementById("display-row");
let demo=document.getElementById("demo");
 
let string;
string="";
let operandCount=0;
let operand=new Array();
clear.onclick=function()
{
    demo.innerHTML="";
    string="";
    operator="";
}

number1.onclick=function()
{
    string=string+"1";
    demo.innerHTML=string;
}

number2.onclick=function()
{
    string=string+"2";
    demo.innerHTML=string;
}
number3.onclick=function()
{
    string=string+"3";
    demo.innerHTML=string;
}
number4.onclick=function()
{
    string=string+"4";
    demo.innerHTML=string;
}
number5.onclick=function()
{
    string=string+"5";
    demo.innerHTML=string;
}
number6.onclick=function()
{
    string=string+"6";
    demo.innerHTML=string;
}
number7.onclick=function()
{
    string=string+"7";
    demo.innerHTML=string;
}
number8.onclick=function()
{
    string=string+"8";
    demo.innerHTML=string;
}
number9.onclick=function()
{
    string=string+"9";
    demo.innerHTML=string;
}
number0.onclick=function()
{
    string=string+"0";
    demo.innerHTML=string;
}
plus.onclick=function()
{
    string=string+"+";
    demo.innerHTML=string;
}
minus.onclick=function()
{
    string=string+"-";
    demo.innerHTML=string;
}
into.onclick=function()
{
    string=string+"*";
    demo.innerHTML=string;
}
quotient.onclick=function()
{
    string=string+"/";
    demo.innerHTML=string;
}
equalsto.onclick=function()
{
    let expn=string;
    let i=0;
    let operatorCount=0;
    let operatorArray=new Array();
    let indexOperatorArray=0;
    for( i=0;i<expn.length;i++)
    {
        let characterValue=expn.charCodeAt(i);
        if(characterValue===42||characterValue===43||characterValue===45||characterValue===47||characterValue===46)
        {
            operatorArray[indexOperatorArray]=expn.charAt(i);
            indexOperatorArray++;
            operatorCount++;
        }
    }
    let operandArray = new Array(operatorCount+1);
    let indexOperandArray=0;
    let j=0;
    let operand="";
    for(j=0;j<expn.length;j++)
    {
        let characterValue=expn.charCodeAt(j);
        if(characterValue>=48&&characterValue<=57)
        {
            operand=operand+expn.charAt(j);
        }  
        if(characterValue==42||characterValue==43||characterValue==45||characterValue==47||characterValue==46||j==expn.length-1)
        {
            operandArray[indexOperandArray]=operand;
            indexOperandArray++;
            operand=" ";
        }
    }
    let idxOfOperandArray=0;
    let idxOfOperatorArray=0;
    let result=0;
    let input1=parseInt(operandArray[idxOfOperandArray]);
    let input2=parseInt(operandArray[idxOfOperandArray+1]);  
    for(idxOfOperandArray=0;idxOfOperandArray<operandArray.length;idxOfOperandArray++)
    {  
        if(idxOfOperatorArray<operatorArray.length)
        {
            let symbol=operatorArray[idxOfOperatorArray]; 
            if(symbol=="+")
            {
                result=result+input1+input2;
            }
            if(symbol=="-")
            {
                result=result+input1-input2;
            }
            if(symbol=="*")
            {
                result=result+input1*input2;
            }
            if(symbol=="/")
            {
                result=result+input1/input2;
            }
            idxOfOperatorArray++;
            input1=input2;
            input2=result;
        }
    }
    demo.innerHTML=result;
    string="";
}
